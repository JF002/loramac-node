#include "gpio-board.h"
#include "board-config.h"
#include "rtc-board.h"
#include "utilities.h"
#include <dirent.h>
#include <fcntl.h>
#include <linux/gpio.h>
#include <linux/spi/spidev.h>
#include <linux/types.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>

static Gpio_t *GpioIrq[16];

struct gpiohandle_request resetGpio;
struct gpiohandle_request busyGpio;
void GpioMcuInit( Gpio_t *obj, PinNames pin, PinModes mode, PinConfigs config, PinTypes type, uint32_t value )
{
  //printf("GpioMcuInit(pin=%d, mode=%d, config=%d, type=%d, value=%d)\n", pin, mode, config, type, value);
  obj->pin = pin;

  struct gpiochip_info info;
  int fd = open("/dev/gpiochip2", O_RDWR);

  if(pin == RADIO_BUSY) {
    busyGpio.flags = GPIOHANDLE_REQUEST_INPUT;
    busyGpio.lines = 1;
    busyGpio.lineoffsets[0] = 8;
    busyGpio.default_values[0] = value;
    strcpy(busyGpio.consumer_label, "LoRa SPI driver");
    ioctl(fd, GPIO_GET_LINEHANDLE_IOCTL, &busyGpio);
  } else if(pin == RADIO_RESET) {
    resetGpio.flags = GPIOHANDLE_REQUEST_OUTPUT;
    resetGpio.lines = 1;
    resetGpio.lineoffsets[0] = 12;
    if(mode == PIN_OUTPUT)
      resetGpio.default_values[0] = value;
    else
      resetGpio.default_values[0] = 1;
    strcpy(resetGpio.consumer_label, "LoRa SPI driver");
    ioctl(fd, GPIO_GET_LINEHANDLE_IOCTL, &resetGpio);
    if(mode == PIN_OUTPUT)
      GpioMcuWrite(obj, 0);
    else
      GpioMcuWrite(obj, 1);

  }
}

void GpioMcuSetContext( Gpio_t *obj, void* context )
{
  printf("GpioMcuSetContext()\n");
  obj->Context = context;
}

void GpioMcuSetInterrupt( Gpio_t *obj, IrqModes irqMode, IrqPriorities irqPriority, GpioIrqHandler *irqHandler )
{
  printf("GpioMcuSetInterrupt()\n");
  // TODO
}

void GpioMcuRemoveInterrupt( Gpio_t *obj )
{
  printf("GpioMcuRemoveInterrupt()\n");
  // TODO
}

void GpioMcuWrite( Gpio_t *obj, uint32_t value )
{
  if(obj->pin == RADIO_RESET) {
    struct gpiohandle_data data;
    data.values[0] = value;
    auto res = ioctl(resetGpio.fd, GPIOHANDLE_SET_LINE_VALUES_IOCTL, &data);
    if(res != 0)
      printf("IOCTL error (GPIOHANDLE_SET_LINE_VALUES_IOCTL failed)\n");
    usleep(1000);

  }
}

void GpioMcuToggle( Gpio_t *obj )
{
  printf("GpioMcuToggle()\n");
    // TODO
}

uint32_t GpioMcuRead( Gpio_t *obj )
{
  if(obj->pin == RADIO_BUSY) {
    struct gpiohandle_data data;
    memset(&data, 0, sizeof(data));
    auto res = ioctl(busyGpio.fd, GPIOHANDLE_GET_LINE_VALUES_IOCTL, &data);
    if(res != 0)
      printf("IOCTL error (GPIOHANDLE_GET_LINE_VALUES_IOCTL failed)\n");

    usleep(1000);

    int value = data.values[0];

    return value;
  }
  return 0;
}
