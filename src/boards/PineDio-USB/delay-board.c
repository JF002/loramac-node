#include "delay-board.h"
#include <unistd.h>

void DelayMsMcu( uint32_t ms )
{
    usleep(ms * 1000);
}
